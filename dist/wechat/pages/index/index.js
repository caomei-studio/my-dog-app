webpackJsonp([3],{

/***/ 110:
/*!**********************************!*\
  !*** ./src/pages/index/index.js ***!
  \**********************************/
/*! dynamic exports provided */
/*! all exports used */
/*! ModuleConcatenation bailout: Module is not an ECMAScript module */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Page({
  data: {
    appId: '',
    saveAvatarUrl: '',
    saveDogUrl: '',
    saveTextUrl: '',
    jumpFlag: false,
    hasPlay: false,
    hasDelay: false,
    userInfo: {}
  },
  letsgo: function letsgo() {
    var _this = this;

    if (!this.data.userInfo.nickName) {
      wx.showToast({
        icon: 'none',
        title: '需要先授权'
      });
      wx.openSetting({
        success: function success() {
          _this.getUserInfo();
        }
      });
      return;
    }
    wx.showLoading({
      title: '正在摇签'
    });
    setTimeout(function () {
      _this.setData({
        hasDelay: true
      });
      _this.jumpTo();
    }, 2000);
    wx.playBackgroundAudio({
      dataUrl: 'https://static.caomeihy.com/dogyear/music.mp3',
      title: '摇一摇',
      coverImgUrl: '',
      complete: function complete() {
        _this.setData({
          hasPlay: true
        });
        _this.jumpTo();
      }
    });
    var list = wx.getStorageSync('list');
    if (!list) {
      wx.request({
        url: 'https://static.caomeihy.com/dogyear/data.json',
        success: function success(res) {
          list = res.data.list;
          wx.setStorage({
            key: 'list',
            data: res.data.list
          });
          console.log(list);
          var num = _this.getRandomImage(list.length);
          console.log(num);
          wx.setStorage({
            key: 'mynum',
            data: num
          });
          _this.downLoadImages(num);
        }
      });
    } else {
      var num = this.getRandomImage(list.length);
      wx.setStorage({
        key: 'mynum',
        data: num
      });
      this.downLoadImages(num);
    }

    // wx.navigateTo({
    //   url: '../edit/edit',
    // });
  },
  onShow: function onShow() {
    wx.startAccelerometer();
    if (!this.data.userInfo.nickName) {
      this.getUserInfo();
    }
  },
  onLoad: function onLoad() {
    var _this2 = this;

    wx.showShareMenu({
      withShareTicket: true
    });
    this.setData({
      userInfo: getApp().userInfo
    });
    wx.startAccelerometer();
    wx.onAccelerometerChange(function (res) {
      if (res.x > 0.3 && res.y > 0.3) {
        if (_this2.data.userInfo) {
          wx.stopAccelerometer();
          _this2.letsgo();
        } else {
          wx.showToast({
            title: '需要先授权'
          });
        }
      }
    });
  },
  onShareAppMessage: function onShareAppMessage() {
    return {
      imageUrl: '../../assets/share.png',
      path: '/pages/index/index',
      title: '神预测，快看看你的狗年签'
    };
  },
  getRandomImage: function getRandomImage(leng) {
    if (this.data.userInfo.nickName) {
      var nickName = this.data.userInfo.nickName;
      var num1 = nickName.charCodeAt(0) % leng;
      var num2 = Math.ceil(nickName.charCodeAt(0) / leng) % leng;
      var res = Math.random() * 10 >= 5 ? num2 : num1;
      return res;
    }
    return Math.floor(Math.random() * leng);
  },
  jumpTo: function jumpTo() {
    var _this3 = this;

    // 头像， 狗套，文字下载在本地，还没跳转，延时是否达到，音乐是否播放
    if (this.data.saveAvatarUrl && this.data.saveDogUrl && this.data.hasDelay && this.data.saveTextUrl && !this.data.jumpFlag && this.data.hasPlay) {
      this.setData({
        jumpFlag: true
      });
      wx.hideLoading();
      wx.navigateTo({
        url: '../edit/edit',
        success: function success() {
          _this3.setData({
            jumpFlag: false,
            hasPlay: false,
            hasDelay: false
          });
        }
      });
    }
  },
  downLoadImages: function downLoadImages(num) {
    var _this4 = this;

    // TODO: 文件存入本地
    // 下载头像
    if (this.data.userInfo.avatarUrl) {
      var hasDownLoadAvatarUrl = wx.getStorageSync('saveAvatarUrl');
      if (!hasDownLoadAvatarUrl) {
        wx.downloadFile({
          url: this.data.userInfo.avatarUrl,
          success: function success(ret) {
            wx.saveFile({
              tempFilePath: ret.tempFilePath,
              success: function success(retu) {
                _this4.setData({
                  saveAvatarUrl: retu.savedFilePath
                });
                wx.setStorage({
                  key: 'saveAvatarUrl',
                  data: retu.savedFilePath
                });
                _this4.jumpTo();
              }
            });
          }
        });
      } else {
        this.setData({
          saveAvatarUrl: hasDownLoadAvatarUrl
        });
        this.jumpTo();
      }
    }
    var imageUrl = 'https://static.caomeihy.com/dogyear/' + num + '/';
    // 下载狗套
    var hasDownLoadDogUrl = wx.getStorageSync('saveDogUrl_' + num);
    if (!hasDownLoadDogUrl) {
      wx.downloadFile({
        url: imageUrl + 'dog.png',
        success: function success(ret) {
          wx.saveFile({
            tempFilePath: ret.tempFilePath,
            success: function success(retu) {
              _this4.setData({
                saveDogUrl: retu.savedFilePath
              });
              wx.setStorage({
                key: 'saveDogUrl_' + num,
                data: retu.savedFilePath
              });
              _this4.jumpTo();
            }
          });
        }
      });
    } else {
      this.setData({
        saveDogUrl: hasDownLoadDogUrl
      });
      this.jumpTo();
    }
    this.setData({
      saveTextUrl: '../../assets/text/' + num + '.png'
    });
    wx.setStorage({
      key: 'saveTextUrl_' + num,
      data: '../../assets/text/' + num + '.png'
    });
    //   // 下载文字
    //   const hasDownLoadTextUrl = wx.getStorageSync(`saveTextUrl_${num}`);
    //   if (!hasDownLoadTextUrl) {
    //     wx.downloadFile({
    //       url: `${imageUrl}text.png`,
    //       success: (ret) => {
    //         wx.saveFile({
    //           tempFilePath: ret.tempFilePath,
    //           success: (retu) => {
    //             this.setData({
    //               saveTextUrl: retu.savedFilePath,
    //             });
    //             wx.setStorage({
    //               key: `saveTextUrl_${num}`,
    //               data: retu.savedFilePath,
    //             });
    //             this.jumpTo();
    //           },
    //         });
    //       },
    //     });
    //   } else {
    //     this.setData({
    //       saveTextUrl: hasDownLoadTextUrl,
    //     });
    //     this.jumpTo();
    //   }
  },
  getUserInfo: function getUserInfo() {
    var _this5 = this;

    wx.getUserInfo({
      success: function success(res) {
        _this5.setData({
          userInfo: res.userInfo
        });
        wx.setStorage({
          key: 'userInfo',
          data: res.userInfo
        });
      }
    });
  }
});

/***/ })

},[110]); function webpackJsonp() { require("./../../common.js"); wx.webpackJsonp.apply(null, arguments); };
//# sourceMappingURL=index.js.map