webpackJsonp([4],{

/***/ 111:
/*!********************************!*\
  !*** ./src/pages/edit/edit.js ***!
  \********************************/
/*! dynamic exports provided */
/*! all exports used */
/*! ModuleConcatenation bailout: Module is not an ECMAScript module */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Page({
  data: {
    userInfo: {},
    saveAvatarUrl: '',
    saveDogUrl: '',
    saveTextUrl: '',
    windowWidth: 0,
    windowHeight: 0,
    myYear: {},
    appData: [
      // {
      //   appId: 'wx4b6f3d02208d19af',
      //   avatarUrl: 'http://static.xisole.cn/chunlian/avatar/hongweijin.png',
      //   appName: '红围巾',
      // },
    ]
  },
  onLoad: function onLoad() {
    var myYear = wx.getStorageSync('mynum');
    var saveAvatarUrl = wx.getStorageSync('saveAvatarUrl');
    var saveDogUrl = wx.getStorageSync('saveDogUrl_' + myYear);
    var saveTextUrl = wx.getStorageSync('saveTextUrl_' + myYear);
    var res = wx.getSystemInfoSync();
    var list = wx.getStorageSync('list');
    var userInfo = wx.getStorageSync('userInfo');
    this.setData({
      userInfo: userInfo,
      saveAvatarUrl: saveAvatarUrl,
      saveDogUrl: saveDogUrl,
      saveTextUrl: saveTextUrl,
      windowHeight: res.windowHeight,
      windowWidth: res.windowWidth,
      myYear: list[myYear]
    });
    // wx.request({
    //   url: 'https://www.xisole.cn/app/ma/list',
    //   success: (resp) => {
    //     if (resp.data && resp.data.code === 1) {
    //       let list = resp.data.data;
    //       let appList = list.filter(val => val.appId !== 'wxfe119db5c0ae39d6');
    //       this.setData({
    //         appData: appList,
    //       });
    //     }
    //   },
    // });
  },
  showMore: function showMore() {
    wx.navigateTo({
      url: "/pages/more/more"
    });
  },
  goToApp: function goToApp(e) {
    var appId = e.currentTarget.id;
    if (appId) {
      console.log('appId： ', appId);
      wx.navigateToMiniProgram({
        appId: appId,
        success: function success() {
          // 打开成功
          console.log('打开成功');
        }
      });
    }
  },
  saveMyPhoto: function saveMyPhoto() {
    this.drawImage();
  },
  onShareAppMessage: function onShareAppMessage() {
    return {
      imageUrl: '../../assets/share.png',
      path: '/pages/index/index',
      title: '神预测，快看看你的狗年签'
    };
  },
  drawImage: function drawImage() {
    var _this = this;

    var userInfo = this.data.userInfo;
    // 使用 wx.createContext 获取绘图上下文 context
    var ctx = wx.createCanvasContext('my-dog');
    // set background color
    ctx.setFillStyle('#B31402');
    ctx.fillRect(0, 0, this.data.windowWidth, this.data.windowHeight);
    // write name with white
    ctx.save();
    ctx.setFillStyle('white');
    ctx.setFontSize(this.getDistance(48));
    ctx.setTextAlign('center');
    ctx.fillText(userInfo.nickName, this.data.windowWidth / 2, this.getDistance(26 + 48));
    ctx.restore();
    // write yellow subttile
    ctx.save();
    ctx.setFillStyle('#E5A14D');
    ctx.setTextAlign('center');
    ctx.setFontSize(this.getDistance(36));
    ctx.fillText('的本命狗是', this.data.windowWidth / 2, this.getDistance(84 + 36));
    ctx.restore();
    // draw bg image
    ctx.drawImage('../../assets/bg.png', 0, this.getDistance(139), this.data.windowWidth, this.getDistance(727));
    ctx.drawImage(this.data.saveTextUrl, 0, this.getDistance(139), this.data.windowWidth, this.getDistance(724));
    // draw user icon
    ctx.drawImage(this.data.saveAvatarUrl, this.getDistance(278), this.getDistance(450), this.getDistance(194), this.getDistance(194));
    ctx.drawImage(this.data.saveDogUrl, this.getDistance(208), this.getDistance(380), this.getDistance(334), this.getDistance(334));
    // write yellow subttile
    ctx.drawImage('../../assets/resbtn.png', this.getDistance(118), this.getDistance(941), this.getDistance(513), this.getDistance(180));
    ctx.save();
    ctx.beginPath();
    ctx.arc(this.data.windowWidth / 2, this.getDistance(887 + 121 / 2), this.getDistance(121 / 2), 0, 2 * Math.PI);
    ctx.clip();
    ctx.setFillStyle('#FFFFFF');
    ctx.fill();
    ctx.drawImage('../../assets/qcode.jpg', this.getDistance(320), this.getDistance(894), this.getDistance(108), this.getDistance(108));
    ctx.restore();

    ctx.save();
    ctx.setFillStyle('#EFB45A');
    ctx.setFontSize(this.getDistance(36));
    ctx.setTextAlign('center');
    ctx.fillText(this.data.myYear.title, this.getDistance(410), this.getDistance(1076));
    ctx.restore();
    ctx.draw(false, function () {
      wx.canvasToTempFilePath({
        canvasId: 'my-dog',
        success: function success(res) {
          wx.saveImageToPhotosAlbum({
            filePath: res.tempFilePath,
            x: 0,
            y: 0,
            width: _this.data.windowWidth,
            height: _this.data.windowHeight,
            destWidth: _this.data.windowWidth,
            destHeight: _this.data.windowHeight,
            success: function success() {
              wx.showModal({
                title: '保存成功',
                content: '到相册查看',
                showCancel: false
              });
            },
            fail: function fail(err) {
              if (err.errMsg === 'saveImageToPhotosAlbum:fail auth deny') {
                wx.openSetting();
              }
            }
          });
        },
        fail: function fail(err) {
          wx.showToast({
            title: err.errMsg
          });
        }
      });
    });
  },
  getDistance: function getDistance(dis) {
    return Math.round(dis * this.data.windowWidth / 750);
  }
});

/***/ })

},[111]); function webpackJsonp() { require("./../../common.js"); wx.webpackJsonp.apply(null, arguments); };
//# sourceMappingURL=edit.js.map