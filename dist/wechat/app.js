webpackJsonp([1],{

/***/ 109:
/*!********************!*\
  !*** ./src/app.js ***!
  \********************/
/*! dynamic exports provided */
/*! all exports used */
/*! ModuleConcatenation bailout: Module is not an ECMAScript module */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


App({
    globalData: {
        appId: "",
        fromAppId: "",
        isInit: false
    },
    onLaunch: function onLaunch(options) {
        var _this = this;

        if (options && options.referrerInfo && options.referrerInfo.extraData) {
            var extraData = options.referrerInfo.extraData;
            this.globalData.appId = extraData.appId;
            this.globalData.fromAppId = options.referrerInfo.appId;
        } else {
            wx.getSetting({
                success: function success(res) {
                    if (!res.authSetting['scope.userInfo']) {
                        wx.authorize({
                            scope: 'scope.userInfo',
                            success: function success() {
                                wx.getUserInfo({
                                    success: function success(ret) {
                                        _this.userInfo = ret.userInfo;
                                        wx.setStorage({
                                            key: 'userInfo',
                                            data: ret.userInfo
                                        });
                                    }
                                });
                            }
                        });
                    } else {
                        wx.getUserInfo({
                            success: function success(ret) {
                                _this.userInfo = ret.userInfo;
                                wx.setStorage({
                                    key: 'userInfo',
                                    data: ret.userInfo
                                });
                            }
                        });
                    }
                }
            });
        }
        this.globalData.isInit = true;
    },
    onShow: function onShow(options) {
        if (this.globalData.isInit) {
            return;
        }
        if (options && options.referrerInfo && options.referrerInfo.extraData) {
            var extraData = options.referrerInfo.extraData;
            this.globalData.appId = extraData.appId;
            this.globalData.fromAppId = options.referrerInfo.appId;
        }
    },
    onError: function onError(err) {
        console.error(err);
    },

    userInfo: {}
});

/***/ }),

/***/ 58:
/*!**********************************************************************************************!*\
  !*** multi es6-promise/dist/es6-promise.auto.min.js ./src/utils/bomPolyfill.js ./src/app.js ***!
  \**********************************************************************************************/
/*! dynamic exports provided */
/*! all exports used */
/*! ModuleConcatenation bailout: Module is not an ECMAScript module */
/***/ (function(module, exports, __webpack_require__) {

__webpack_require__(/*! es6-promise/dist/es6-promise.auto.min.js */59);
__webpack_require__(/*! ./src/utils/bomPolyfill.js */61);
module.exports = __webpack_require__(/*! ./src/app.js */109);


/***/ })

},[58]); function webpackJsonp() { require("./common.js"); wx.webpackJsonp.apply(null, arguments); };
//# sourceMappingURL=app.js.map