const app = getApp();

Page({
    data: {
        fromAppId: "",
        isRedirected: false,
        appData: [
            // {
            //   appId: 'wx4b6f3d02208d19af',
            //   avatarUrl: 'http://static.xisole.cn/chunlian/avatar/hongweijin.png',
            //   appName: '红围巾',
            // },
        ],
    },
    onLoad() {
        this.isRedirect()
    },
    isRedirect() {
        let appId = app.globalData.appId;
        let fromAppId = app.globalData.fromAppId;
        let self = this;
        if (appId && wx.navigateToMiniProgram && !self.data.isRedirected) {
            self.setData({
                isRedirected: true,
                fromAppId: fromAppId,
                appId: appId,
            });
            wx.navigateToMiniProgram({
                appId: appId,
                success(res) {
                    console.log("打开成功");
                }
            })
        } else {
            wx.request({
                url: `https://www.xisole.cn/app/ma/recommend_list?appId=wxfe119db5c0ae39d6`,
                success: (resp) => {
                    const { data: { code, data } } = resp;
                    if (+code === 1) {
                        this.setData({
                            appData: data.wxAppVOs || [],
                        });
                    }
                },
            });
        }
    },
    onShow() {
        if(this.data.appData.length) {
            return
        }
        this.isRedirect()
    },
    goToApp(e) {
        const appId = e.currentTarget.id;
        if (appId) {
            console.log('appId： ', appId);
            wx.navigateToMiniProgram({
                appId: appId,
                success() {
                    // 打开成功
                    console.log('打开成功');
                },
            });
        }
    },

    onShareAppMessage() {
        return {
            path: '/pages/more/more',
            title: '这里有好多好玩的小程序推荐给你',
        };
    },
});
