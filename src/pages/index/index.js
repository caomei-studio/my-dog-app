Page({
  data: {
    appId: '',
    saveAvatarUrl: '',
    saveDogUrl: '',
    saveTextUrl: '',
    jumpFlag: false,
    hasPlay: false,
    hasDelay: false,
    userInfo: {},
  },
  letsgo() {
    if (!this.data.userInfo.nickName) {
      wx.showToast({
        icon: 'none',
        title: '需要先授权',
      });
      wx.openSetting({
        success: () => {
          this.getUserInfo();
        },
      });
      return;
    }
    wx.showLoading({
      title: '正在摇签',
    });
    setTimeout(() => {
      this.setData({
        hasDelay: true,
      });
      this.jumpTo();
    }, 2000);
    wx.playBackgroundAudio({
      dataUrl: 'https://static.caomeihy.com/dogyear/music.mp3',
      title: '摇一摇',
      coverImgUrl: '',
      complete: () => {
        this.setData({
          hasPlay: true,
        });
        this.jumpTo();
      },
    });
    let list = wx.getStorageSync('list');
    if (!list) {
      wx.request({
        url: 'https://static.caomeihy.com/dogyear/data.json',
        success: (res) => {
          list = res.data.list;
          wx.setStorage({
            key: 'list',
            data: res.data.list,
          });
          console.log(list);
          const num = this.getRandomImage(list.length);
          console.log(num);
          wx.setStorage({
            key: 'mynum',
            data: num,
          });
          this.downLoadImages(num);
        },
      });
    } else {
      const num = this.getRandomImage(list.length);
      wx.setStorage({
        key: 'mynum',
        data: num,
      });
      this.downLoadImages(num);
    }

    // wx.navigateTo({
    //   url: '../edit/edit',
    // });
  },
  onShow() {
    wx.startAccelerometer();
    if (!this.data.userInfo.nickName) {
      this.getUserInfo();
    }
  },

  onLoad() {
    wx.showShareMenu({
      withShareTicket: true,
    });
    this.setData({
      userInfo: getApp().userInfo,
    });
    wx.startAccelerometer();
    wx.onAccelerometerChange((res) => {
      if (res.x > 0.3 && res.y > 0.3) {
        if (this.data.userInfo) {
          wx.stopAccelerometer();
          this.letsgo();
        } else {
          wx.showToast({
            title: '需要先授权',
          });
        }
      }
    });
  },
  onShareAppMessage() {
    return {
      imageUrl: '../../assets/share.png',
      path: '/pages/index/index',
      title: '神预测，快看看你的狗年签',
    };
  },
  getRandomImage(leng) {
    if (this.data.userInfo.nickName) {
      let nickName = this.data.userInfo.nickName;
      const num1 = nickName.charCodeAt(0) % leng;
      const num2 = Math.ceil(nickName.charCodeAt(0) / leng) % leng;
      const res = Math.random() * 10 >= 5 ? num2 : num1;
      return res;
    }
    return Math.floor(Math.random() * leng);
  },
  jumpTo() {
    // 头像， 狗套，文字下载在本地，还没跳转，延时是否达到，音乐是否播放
    if (this.data.saveAvatarUrl && this.data.saveDogUrl && this.data.hasDelay
      && this.data.saveTextUrl && !this.data.jumpFlag && this.data.hasPlay) {
      this.setData({
        jumpFlag: true,
      });
      wx.hideLoading();
      wx.navigateTo({
        url: '../edit/edit',
        success: () => {
          this.setData({
            jumpFlag: false,
            hasPlay: false,
            hasDelay: false,
          });
        },
      });
    }
  },
  downLoadImages(num) {
    // TODO: 文件存入本地
    // 下载头像
    if (this.data.userInfo.avatarUrl) {
      const hasDownLoadAvatarUrl = wx.getStorageSync('saveAvatarUrl');
      if (!hasDownLoadAvatarUrl) {
        wx.downloadFile({
          url: this.data.userInfo.avatarUrl,
          success: (ret) => {
            wx.saveFile({
              tempFilePath: ret.tempFilePath,
              success: (retu) => {
                this.setData({
                  saveAvatarUrl: retu.savedFilePath,
                });
                wx.setStorage({
                  key: 'saveAvatarUrl',
                  data: retu.savedFilePath,
                });
                this.jumpTo();
              },
            });
          },
        });
      } else {
        this.setData({
          saveAvatarUrl: hasDownLoadAvatarUrl,
        });
        this.jumpTo();
      }
    }
    const imageUrl = `https://static.caomeihy.com/dogyear/${num}/`;
    // 下载狗套
    const hasDownLoadDogUrl = wx.getStorageSync(`saveDogUrl_${num}`);
    if (!hasDownLoadDogUrl) {
      wx.downloadFile({
        url: `${imageUrl}dog.png`,
        success: (ret) => {
          wx.saveFile({
            tempFilePath: ret.tempFilePath,
            success: (retu) => {
              this.setData({
                saveDogUrl: retu.savedFilePath,
              });
              wx.setStorage({
                key: `saveDogUrl_${num}`,
                data: retu.savedFilePath,
              });
              this.jumpTo();
            },
          });
        },
      });
    } else {
      this.setData({
        saveDogUrl: hasDownLoadDogUrl,
      });
      this.jumpTo();
    }
    this.setData({
      saveTextUrl: `../../assets/text/${num}.png`,
    });
    wx.setStorage({
      key: `saveTextUrl_${num}`,
      data: `../../assets/text/${num}.png`,
    });
  //   // 下载文字
  //   const hasDownLoadTextUrl = wx.getStorageSync(`saveTextUrl_${num}`);
  //   if (!hasDownLoadTextUrl) {
  //     wx.downloadFile({
  //       url: `${imageUrl}text.png`,
  //       success: (ret) => {
  //         wx.saveFile({
  //           tempFilePath: ret.tempFilePath,
  //           success: (retu) => {
  //             this.setData({
  //               saveTextUrl: retu.savedFilePath,
  //             });
  //             wx.setStorage({
  //               key: `saveTextUrl_${num}`,
  //               data: retu.savedFilePath,
  //             });
  //             this.jumpTo();
  //           },
  //         });
  //       },
  //     });
  //   } else {
  //     this.setData({
  //       saveTextUrl: hasDownLoadTextUrl,
  //     });
  //     this.jumpTo();
  //   }
  },
  getUserInfo() {
    wx.getUserInfo({
      success: (res) => {
        this.setData({
          userInfo: res.userInfo,
        });
        wx.setStorage({
          key: 'userInfo',
          data: res.userInfo,
        });
      },
    });
  },
});
