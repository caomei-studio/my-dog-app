import _ from 'lodash';

const wxApis = apiName => options => new Promise((resolve, reject) => {
  wx[apiName](_.assign({}, options, {
    success: resolve,
    fail(err) {
      reject(new Error(err.errMsg));
    },
  }));
});

export const showModal = wxApis('showModal');
export const chooseVideo = wxApis('chooseVideo');
export const uploadFile = wxApis('uploadFile');
export const saveVideoToPhotosAlbum = wxApis('saveVideoToPhotosAlbum');

wxApis.showModal = showModal;
wxApis.chooseVideo = chooseVideo;
wxApis.uploadFile = uploadFile;
wxApis.saveVideoToPhotosAlbum = saveVideoToPhotosAlbum;

export default wxApis;
