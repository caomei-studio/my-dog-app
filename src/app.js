App({
    globalData: {
        appId: "",
        fromAppId: "",
        isInit: false
    },
    onLaunch(options) {
        if (options && options.referrerInfo && options.referrerInfo.extraData) {
            let extraData = options.referrerInfo.extraData;
            this.globalData.appId = extraData.appId;
            this.globalData.fromAppId = options.referrerInfo.appId;
        } else {
            wx.getSetting({
                success: (res) => {
                    if (!res.authSetting['scope.userInfo']) {
                        wx.authorize({
                            scope: 'scope.userInfo',
                            success: () => {
                                wx.getUserInfo({
                                    success: (ret) => {
                                        this.userInfo = ret.userInfo;
                                        wx.setStorage({
                                            key: 'userInfo',
                                            data: ret.userInfo,
                                        });
                                    },
                                });
                            },
                        });
                    } else {
                        wx.getUserInfo({
                            success: (ret) => {
                                this.userInfo = ret.userInfo;
                                wx.setStorage({
                                    key: 'userInfo',
                                    data: ret.userInfo,
                                });
                            },
                        });
                    }
                },
            });
        }
        this.globalData.isInit = true
    },
    onShow(options) {
        if (this.globalData.isInit) {
            return
        }
        if (options && options.referrerInfo && options.referrerInfo.extraData) {
            let extraData = options.referrerInfo.extraData;
            this.globalData.appId = extraData.appId;
            this.globalData.fromAppId = options.referrerInfo.appId;
        }
    },
    onError(err) {
        console.error(err);
    },
    userInfo: {},
});
